package facture;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;

public class Facture {

    private Client destinataire;
    private Date emission;
    private int numero;
    private LinkedList<LigneFacture> lignes;

    public Facture(Client destinataire, Date emission, int numero) {
         this.destinataire=destinataire;
         this.emission=emission;
         this.numero=numero;
         lignes=new LinkedList<LigneFacture>();
    }

    /**
     * Get the value of numero
     *
     * @return the value of numero
     */
    public int getNumero() {
         return numero;
    }

    /**
     * Get the value of emission
     *
     * @return the value of emission
     */
    public Date getEmission() {
         return emission;
    }

    /**
     * Get the value of destinataire
     *
     * @return the value of destinataire
     */
    public Client getDestinataire() {
         return destinataire;
    }

    public void ajouteLigne(Article a, int nombre, float remise) {
         LigneFacture l = new LigneFacture(nombre,this,a,remise);
         lignes.add(l);
   }
    
   public float montantTTC(float tauxTVA) {
        float result=0;
         for (LigneFacture l : lignes){
             result=result+(l.montantLigne());
         }
         return result* (1+tauxTVA);
   }
   
   public void print(PrintStream out, float tva) {
       out.println("Client : "+this.getDestinataire());
       out.println("Facture n°: "+this.getNumero() + "Taux de TVA" + tva);
       for(LigneFacture l : lignes){
           out.println(l.toString());
       }
   }
   
}
