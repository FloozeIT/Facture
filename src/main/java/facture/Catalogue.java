package facture;

import java.util.HashMap;
import java.util.Map;

public class Catalogue {

    private  HashMap<String, Article> hmap;

    public Catalogue(){
        hmap = new HashMap<String, Article>();
    }
    
    public void addArticle(Article article) {
        hmap.put(article.getCode(),article);
    }
    
    public Article findByCode(String code) {
        for(Map.Entry<String, Article> entry : hmap.entrySet()) {
            if(entry.getKey()==code){
                return entry.getValue();}
        }return null;
    }
    
}
